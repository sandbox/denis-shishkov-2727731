This module allows to inject any peace of content between views results.
It uses standard views global area plugins like as text area or views results.
This module also includes additional global area plugin to inject blocks.
So it can be used to place blocks in the header/footer/empty areas.
