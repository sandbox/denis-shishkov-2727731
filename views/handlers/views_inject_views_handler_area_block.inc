<?php

/**
 * @file
 * Renders a block in a views area.
 */

/**
 * Area handler for rendering blocks.
 */
class views_inject_views_handler_area_block extends views_handler_area {
  function option_definition() {
    $options['block'] = array('default' => '');
    $options['label'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $blocks = _block_rehash();
    $options = array();
    foreach ($blocks as $block) {
      $id = $block['module'] . '$' . $block['delta'];
      $options[$id] = $block['info'];
    }
    $form['block'] = array(
      '#type' => 'select',
      '#title' => t('Block'),
      '#options' => $options,
      '#default_value' => $this->options['block'],
    );
  }

  function options_submit(&$form, &$form_state) {
    $form_state['values']['options']['label'] = $form['block']['#options'][$form_state['values']['options']['block']];
    parent::options_submit($form, $form_state);
  }

  function render($empty = FALSE) {

    list($module, $delta) = explode('$', $this->options['block']);

    $block = module_invoke($module, 'block_view', $delta);
    if ($block) {
      return render($block['content']);
    }

    return '';
  }
}
