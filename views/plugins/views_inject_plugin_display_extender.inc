<?php

/**
 * @file
 * Custom display extender plugin for Views.
 */

/**
 * Views display extender class for rendering content in views results.
 */
class views_inject_plugin_display_extender extends views_plugin_display_extender {

  /**
   * Default values.
   */
  function options_definition() {
    $options = parent::option_definition();
    $options['injects'] = array('default' => array());
    return $options;
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_definition_alter(&$options) {
    $options['injects'] = array('default' => array());
  }

  /**
   * Defines where within the Views admin UI the new settings will be visible.
   */
  function options_summary(&$categories, &$options) {
    $categories['injects'] = array(
      'title' => t('Injects'),
      'column' => 'second',
      'build' => views_inject_edit_form_get_bucket($this->view, $this->display->display),
    );
  }
}
