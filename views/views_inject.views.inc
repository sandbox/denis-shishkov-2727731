<?php

/**
 * @file
 * Views integration for the views_inject module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_inject_views_plugins() {
  $plugins = array();
  $plugins['display_extender']['injects'] = array(
    'title' => t('Injects'),
    'help' => t('Injects pieces of content inside views results.'),
    'handler' => 'views_inject_plugin_display_extender',
    'enabled' => TRUE,
    'path' => drupal_get_path('module', 'views_inject') . '/views/plugins',
  );

  return $plugins;
}

/**
 * Implements hook_views_data().
 */
function views_inject_views_data() {
  $data['inject']['table']['group'] = t('Global');
  $data['inject']['table']['join'] = array(
    '#global' => array(),
  );
  $data['inject']['block'] = array(
    'title' => t('Block'),
    'help' => t('Displays a block.'),
    'area' => array(
      'handler' => 'views_inject_views_handler_area_block',
    ),
  );

  return $data;
}
