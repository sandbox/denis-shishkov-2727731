<?php

/**
 * @file
 * Implements main functionality for the views_inject module.
 */

/**
 * Inject item add form.
 */
function views_inject_add_form($form, &$form_state) {

  $form = views_ui_add_item_form($form, $form_state);
  $form['buttons']['submit']['#submit'][] = 'views_inject_add_form_submit';

  return $form;
}

/**
 * Submit handler for adding new injection item(s) to a view.
 */
function views_inject_add_form_submit($form, &$form_state) {
  $form_state['view']->stack[0][1] = 'config-inject-item';
}

/**
 * Inject item configuration form.
 */
function views_inject_config_form($form, &$form_state) {
  $form = views_ui_config_item_form($form, $form_state);

  $view = &$form_state['view'];
  $display_id = $form_state['display_id'];
  $type = $form_state['type'];
  $id = $form_state['id'];

  $item = $view->get_item($display_id, $type, $id);

  $form['options']['inject'] = array(
    '#type' => 'fieldset',
    '#title' => t('Injection'),
    '#weight' => -1,
    '#tree' => TRUE,
  );
  $form['options']['inject']['position'] = array(
    '#title' => t('Position'),
    '#type' => 'textfield',
    '#default_value' => isset($item['inject']['position']) ? $item['inject']['position'] : 0,
  );

  $form['options']['inject']['class'] = array(
    '#title' => t('Item class'),
    '#type' => 'textfield',
    '#default_value' => isset($item['inject']['class']) ? $item['inject']['class'] : 'views-inject-content',
  );

  $form['buttons']['submit']['#submit'][] = 'views_ui_config_item_form_submit';
  $form['buttons']['submit']['#submit'][] = 'views_inject_config_form_submit_final';

  return $form;
}

/**
 * Submit handler for configuring injection item.
 */
function views_inject_config_form_submit_final($form, &$form_state) {
  $item = $form_state['view']->get_item($form_state['display_id'], $form_state['type'], $form_state['id']);
  $item['inject'] = $form_state['values']['options']['inject'];

  $form_state['view']->set_item($form_state['display_id'], $form_state['type'], $form_state['id'], $item);
  views_ui_cache_set($form_state['view']);
}

/**
 * Add information about a section to a display.
 */
function views_inject_edit_form_get_bucket($view, $display) {
  $build = views_ui_edit_form_get_bucket('inject', $view, $display);

  foreach ($build['fields'] as $id => $field) {
    $build['fields'][$id]['#link'] = str_replace('/config-item/', '/config-inject-item/', $build['fields'][$id]['#link']);
  }
  $build['#actions'] = str_replace('/add-item/', '/add-inject-item/', $build['#actions']);

  unset($build['#theme_wrappers']);

  return $build;
}

/**
 * Implements hook_views_object_types_alter().
 */
function views_inject_views_object_types_alter(&$types) {
  $types['inject'] = array(
    'title' => t('Inject'),
    'ltitle' => t('inject'),
    'stitle' => t('Inject'),
    'lstitle' => t('Inject'),
    'plural' => 'injects',
    'type' => 'area',
  );
}

/**
 * Implements hook_views_ui_ajax_forms_alter().
 */
function views_inject_views_ui_ajax_forms_alter(&$forms) {
  $forms += array(
    'add-inject-item' => array(
      'form_id' => 'views_inject_add_form',
      'args' => array('type', 'id'),
    ),
    'config-inject-item' => array(
      'form_id' => 'views_inject_config_form',
      'args' => array('type', 'id'),
    ),
  );
}

/**
 * Implements hook_views_api().
 */
function views_inject_views_api() {
  return array(
    'api' => '3.0',
    'path' => drupal_get_path('module', 'views_inject') . '/views',
  );
}

/**
 * Implements template_preprocess_views_view().
 */
function views_inject_preprocess_views_view_unformatted(&$vars) {
  $view = $vars['view'];

  if ($injects = $view->display_handler->get_handlers('inject')) {
    $counter = count($vars['rows']);
    $index_offset = 0;

    foreach ($injects as $inject) {
      $content = $inject->render(!$counter);
      $settings = $inject->options['inject'];
      $class = $settings['class'];
      if ($settings['position'] <= $counter) {
        array_splice($vars['rows'], $settings['position'] + $index_offset, NULL, $content);
        array_splice($vars['classes_array'], $settings['position'] + $index_offset, NULL, $class);
        ++$index_offset;
      }
      else {
        $vars['rows'][] = $content;
        $vars['classes_array'][] = $class;
      }
    }
  };
}
